import { Injectable } from '@angular/core';

import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';


/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  public prefix = 'https://rx.optical88.com.hk:3443/api/'
  public session ;
  public mbrCode;
  public token;
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': ''
    })
  };

  constructor(private http: HttpClient , private storage: Storage) {


    this.storage.get('mbrCode').then( (val)=>{

      this.mbrCode=val;
    })


    this.storage.get('token').then( (val)=>{
      if(val){
        this.httpOptions.headers=new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': val
        })
      }
      
    })

   
  }
  checkLogined(){

    this.storage.get('token').then( (val)=>{

      if(!val){
        return false
      }else {


      }

    })


  }
  setMemberCode(code){

    this.mbrCode=code
  }
  login(info){


    return this.http.post(this.prefix+"member/login", info, this.httpOptions)
    .pipe(
    ); 

    
  }
  signup(info){

    
    


    return this.http.post(this.prefix+"member/signup", info, this.httpOptions)
    .pipe(
     
    );
  }
  

  getHistory(info){
    return this.http.post(this.prefix+"memberRecord", info, this.httpOptions)
    .pipe(
     
    );
  }
  
  

  memberBinding(info){
    return this.http.post(this.prefix+"member/memberBinding", info, this.httpOptions)
    .pipe(
     
    );
  }

  getAllAvaliable(){

    return this.http.get(this.prefix+"memberRecord/allAvailable/"+ this.mbrCode,this.httpOptions).pipe(
      
      );
  

  }

  verifysms(code){
    return this.http.post(this.prefix+"member/verifysms",code, this.httpOptions)
    .pipe(
     
    );

  }
  
}
 


