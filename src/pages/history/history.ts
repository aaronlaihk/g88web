import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import { NoresultPage } from '../noresult/noresult';
import { HomePage } from '../home/home';
import {   App} from 'ionic-angular';
import { AlertController , LoadingController } from 'ionic-angular';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  public records 
  constructor(public loadingCtrl: LoadingController,public appCtrl: App , public navCtrl: NavController, public navParams: NavParams,public api:ApiProvider,public alert : AlertController) {

    console.log(navParams)
  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      content: '',
      showBackdrop:true
    });
  
    loading.present();
  


    this.api.getHistory(this.navParams.data).subscribe( (result)=>{
      loading.dismiss();

      this.records=result
      if(result==0){
        this.navCtrl.setRoot(NoresultPage)

      }

      console.log(result)
    })

  }

  formatDateString(string){

    return string.substr(0,10)
  }

  bind(record){
    let info = this.navParams.data
    info.mbr_code = record.user_member
    console.log(info);
    this.api.memberBinding(info).subscribe( (result)=>{

  
      console.log(result)
      if(result['rowsAffected'][0]==1){
        let alert = this.alert.create({
          title: '成功',
          subTitle: '已成功綁定賬戶, 請重新登入！',
          buttons: ['Ok']
        });
        alert.present();
        this.appCtrl.getRootNav().setRoot(HomePage);
      }
    })
    

  }


}
