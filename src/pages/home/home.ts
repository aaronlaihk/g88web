import { Component } from '@angular/core';
import { NavController , App} from 'ionic-angular';
import { HistoryPage } from '../history/history';
import { ResultPage } from '../result/result';
import {ApiProvider} from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { AlertController , LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public info 
  public login 
  public area 
  public code
  public canReSms= false
  public smsTime = 60
  constructor(public loadingCtrl: LoadingController,public appCtrl: App , public alertCtrl : AlertController , public navCtrl: NavController, public api : ApiProvider,public storage:Storage) {
    this.info={area:"852"
    }

    this.login={area:"852"}
  }



  goHistroy(){

    this.navCtrl.push(HistoryPage,this.info)
  }


  goResult(){
    this.navCtrl.push(ResultPage)
  }

  goLogin(){

    let loading = this.loadingCtrl.create({
      content: '',
      showBackdrop:true
    });
  
    loading.present();
  


    console.log(this.login)


    this.api.login(this.login).subscribe((result) => {
      console.log(result)
      loading.dismiss();

      if(result['status']==1){
        this.storage.set("mbrCode",result['data']['mbrCode'])
        this.storage.set("token",result['data']['token'])

        this.navCtrl.push(ResultPage,result)
        this.api.setMemberCode(result['data']['mbrCode'])

      }else{

        let alert = this.alertCtrl.create({
          title: '錯誤',
          subTitle: '登入失敗，請檢查所輸入的資料!',
          buttons: ['Ok']
        });
        alert.present();


      }
    })
  }

  numberCheck(){

    let result =  false
    if(this.info.area=="852" && this.info.phone.length==8 && (this.info.phone[0]=="5" ||  this.info.phone[0]=="6" || this.info.phone[0]=="7" || this.info.phone[0]=="9") ){
      result = true
    }else     if(this.info.area=="853" && this.info.phone.length==8 && (this.info.phone[0]=="6") ){
      result = true

    }else     if(this.info.area=="86" && this.info.phone.length==11 && (this.info.phone[0]=="1") ){
      result = true

    }


    if(this.info.password.length>=6 && this.info.password <=8){
      result = true

    }
    return result
  }
  signup(){

    
   // this.countSms()

    if (this.numberCheck()==false){
      let alert = this.alertCtrl.create({
        title: '電話或密碼長度錯誤，請從新輸入',
        buttons: ['Ok']
      });
      alert.present();

    }else 
    if(this.info.password != this.info._password){

       let alert = this.alertCtrl.create({
        title: '兩次輸入密碼不相同',
        buttons: ['Ok']
      });
      alert.present();
    }
    else {
     // this.navCtrl.push(HistoryPage,this.info)
      
      let loading = this.loadingCtrl.create({
        content: '',
        showBackdrop:true
      });
    
      loading.present();
      
      this.api.signup(this.info).subscribe((result) => {
        this.countSms()
        console.log(result)
        if(result['status']==1){
        // this.navCtrl.push(HistoryPage,this.info)
         let alert = this.alertCtrl.create({
          title: '驗證碼已發出',
          buttons: ['Ok']
        });
        alert.present();
        loading.dismiss();

      }
  
        
      })

    }

    
  //  this.navCtrl.push(HistoryPage,this.info)
  

  

  }

  verify(){

    let loading = this.loadingCtrl.create({
      content: '',
      showBackdrop:true
    });


    console.log(this.code)

    let query={
      phone : this.info.phone,
      code:this.code

    }
    this.api.verifysms(query).subscribe((result)=>{
        loading.dismiss();

      console.log(result)
      if(result['status']==1){

        let alert = this.alertCtrl.create({
          title: '驗證成功',
          buttons: ['Ok']
        });
        alert.present();



        this.navCtrl.push(HistoryPage,this.info)
         
 
       }else{
        let alert = this.alertCtrl.create({
          title: '驗證失敗',
          buttons: ['Ok']
        });
        alert.present();


       }

    })


  
}

countSms(){

  if(this.smsTime>1){
    this.canReSms=false;
    this.smsTime--
    setTimeout(() => {
      this.countSms()
    }, 1000);
  }else{

    this.canReSms=true;
    this.smsTime=60;
  }
 
}

}
