import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoresultPage } from './noresult';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(NoresultPage),
  ],
})
export class NoresultPageModule {}
