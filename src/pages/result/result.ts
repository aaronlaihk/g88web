import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';

/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {
  public imagePath
  public items
  constructor(public navCtrl: NavController, public navParams: NavParams , public api : ApiProvider) {

    let timeString = (new Date()).getTime()
    this.imagePath = "https://rx.optical88.com.hk:3443/api/code/"  + this.navParams.data.data.mbrCode + "/" + timeString

  }

  ionViewDidLoad() {
    console.log(this.imagePath);
    console.log('ionViewDidLoad ResultPage');

    this.api.getAllAvaliable().subscribe ( (result)=>{
      this.items=result
      console.log(result)


    })
  }

}
